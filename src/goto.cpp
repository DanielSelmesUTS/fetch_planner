#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>

#define USE_MATH_DEFINES
#include <math.h>
#include <string>
#include <iostream>

enum TargetPose : int {
    tuck = 0,
    reach = 1,
    cart = 2,
    error = -1
};

int main(int argc, char** argv)
{
    // Initialise ROS node
    ros::init(argc, argv, "fetch_goto");
    ros::NodeHandle nodeHandle;

    // Parse command line arguments
    TargetPose tp = TargetPose::error;
    std::string tpStr;
    std::vector<float> coords;
    switch (argc) {
        case 4:
            tp = TargetPose::cart;
            coords.push_back(atof(argv[1]));
            coords.push_back(atof(argv[2]));
            coords.push_back(atof(argv[3]));
            ROS_INFO("Going to coords (%.2f, %.2f,%.2f)", coords.at(0), coords.at(1), coords.at(2));
            break;
        case 2:
            tpStr = std::string(argv[1]);
            if (tpStr == "tuck") {tp = TargetPose::tuck;}
            if (tpStr == "reach") {tp = TargetPose::reach;}
            if (tp == TargetPose::error) {
                ROS_ERROR("Invalid pose given.");
                exit(0);
            }
            break;
        case 1:
            printf("Usage: fetch_goto <pose>\npose is one of the following values:\n\ttuck - arm is tucked in front of the robot"
            "\n\treach - ready to do a grab in front of the robot\nOR, a set of XYZ coordinates (in meters)\n");
        default:
            ROS_ERROR("Invalid command line arguments, exiting...");
            exit(0);
    }

    // Start up the ROS background thread
    ros::AsyncSpinner spinner(1);
    spinner.start();

    // Create the move_group_interface to control the robot with
    static const std::string PLANNING_GROUP = "arm";
    moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);

    // Set the target to the desired pose
    switch (tp) {
        case TargetPose::tuck:
            move_group_interface.setJointValueTarget(std::vector<double>(
                {M_PI/2, M_PI/2, 0.0, M_PI/2, 0.0, M_PI/2, 0.0}
                ));
            break;
        case TargetPose::reach:
            move_group_interface.setJointValueTarget(std::vector<double>(
                {0.0, -M_PI/4.0, 0.0, M_PI/4.0, 0.0, M_PI/4.0, 0.0}
            ));
            break;
        case TargetPose::cart:
            // I should probably visualise this
            move_group_interface.setPoseTarget(Eigen::Isometry3d(Eigen::Translation3d(coords.at(0), coords.at(1), coords.at(2))));
            break;
        default:
            ROS_ERROR("Invalid pose given.");
            exit(0);
    }
    // Execute the move
    move_group_interface.move();

    ros::shutdown();
    return 0;

}