#include "ros/ros.h"
#include <Eigen/Geometry>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_visual_tools/moveit_visual_tools.h>

#define _USE_MATH_DEFINES
#include <math.h>

int main(int argc, char** argv)
{
    /* Start the ROS node. */
    ros::init(argc, argv, "fetch_planner");
    ros::NodeHandle node_handle;

    /* Apparently we need to do this to get the robot state. */
    ros::AsyncSpinner spinner(1);
    spinner.start();

    /* Get access to our moveit move_group_interface. */
    static const std::string PLANNING_GROUP = "arm";
    moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);

    /* Start up the visual tools and create a marker in rviz if we're running that. */
    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("base_link");
    visual_tools.deleteAllMarkers();
    Eigen::Isometry3d debug_text_pose = Eigen::Isometry3d::Identity();
    debug_text_pose.translation().z() = 1.5;
    visual_tools.publishText(debug_text_pose, "Fetch Planner Starting", rvt::WHITE, rvt::XXLARGE);
    visual_tools.trigger();

    /* Get the robot joints. */
    std::vector<double> joints = move_group_interface.getCurrentJointValues();
    std::vector<std::string> names = move_group_interface.getJoints();
    ROS_INFO("Initial Joint Values: ");
    for (int i = 0; i < joints.size(); i++) {
        ROS_INFO("\t%s: %f", names[i].c_str(), joints[i]);
    }
    
    /* Move into the starting position. */
    ROS_INFO("Moving to Home Position");
    std::vector<double> jointsHome = {
        0.0, 
        -M_PI / 4.0,
        0.0, 
        M_PI / 4.0,
        0.0,
        M_PI / 4.0,
        0.0
    };
    if (!move_group_interface.setJointValueTarget(jointsHome)) {
        ROS_ERROR("Unable to set home joint state.");
        ros::shutdown();
        exit(-1);
    }
    /* Plan and execute the home move. */
    moveit::planning_interface::MoveItErrorCode res = move_group_interface.move();
    if (!res) {
        ROS_ERROR("Failed to move to home joint state.");
        ros::shutdown();
        exit(-1);
    }


    /* Sleep for a bit to let it all start up? */
    ros::Time::sleepUntil(ros::Time::now() + ros::Duration(5.0));

    /* Cleanup and shutdown. */
    ros::shutdown();
    return 0;
}