#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <trajectory_msgs/JointTrajectory.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "headControl");
    ros::NodeHandle node_handle;
    double pan = 0;
    double tilt = 0;
    int seq = 0;
    
    /* Process the command line for the pan and tilt parameters. */
    switch (argc) {
        case 4:{
            /* Process the pan and tilt parameters. */
            pan = atof(argv[1]);
            tilt = atof(argv[2]);
            seq = atoi(argv[3]);
            break;
        }
        case 1 :{
            printf("Usage: %s pan tilt seq\n\tpan - camera pan joint value\n\ttilt - camera tilt\n\tseq - Sequence value for the action\n", argv[0]);
        } // Yes this does indeed flow down into the default.
        default:{
            /* Display usage info and exit. */
            ROS_ERROR("Invalid command line arguments, exiting...");
            exit(0);
        }
    }

    /* Create the action to move the camera to the desired pose. */
    ROS_INFO("Moving head camera to pan=%.2f, tilt=%.2f, seq=%d", pan, tilt, seq);

    actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> cl("/head_controller/follow_joint_trajectory", true);

    /* Fill in the details of the goal. */
    control_msgs::FollowJointTrajectoryGoal goal;
    goal.trajectory.header.seq = seq;
    goal.trajectory.joint_names.push_back("head_pan_joint");
    goal.trajectory.joint_names.push_back("head_tilt_joint");
    trajectory_msgs::JointTrajectoryPoint pt;
    pt.positions.push_back(pan);
    pt.positions.push_back(tilt);
    pt.velocities.push_back(0);
    pt.velocities.push_back(0);
    pt.time_from_start.sec = 3;
    pt.time_from_start.nsec = 0;
    goal.trajectory.points.push_back(pt);

    actionlib::SimpleClientGoalState result = cl.sendGoalAndWait(goal, ros::Duration(4, 0));
    if (result != actionlib::SimpleClientGoalState::SUCCEEDED) {
        ROS_ERROR("Failed to move head, goal did not succeed.\n");
        ros::shutdown();
        exit(-1);
    }
    ROS_INFO("Head Turn Succeeded");

    /* Cleanup and shutdown. */
    ros::shutdown();
    return 0;
}